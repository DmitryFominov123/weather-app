package com.weather.sampleapp.repository

import com.weather.sampleapp.db.entity.UserLocationEntity
import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity
import com.weather.sampleapp.di.scopes.ApplicationScope
import com.weather.sampleapp.di.scopes.Local
import com.weather.sampleapp.di.scopes.Remote
import com.weather.sampleapp.di.sources.IWeatherLocalDataSource
import com.weather.sampleapp.di.sources.IWeatherRemoteDataSource
import com.weather.sampleapp.utils.ConstUtil.Companion.API_KEY
import com.weather.sampleapp.utils.NetworkUtil
import io.reactivex.Observable
import javax.inject.Inject

@ApplicationScope
class WeatherRepository @Inject constructor(
    @Remote val remoteDataSource: IWeatherRemoteDataSource,
    @Local val localDataSource: IWeatherLocalDataSource
) {

    fun getCurrentWeather(city: String): Observable<CurrentWeatherEntity>? {
        val checkConnection = NetworkUtil.isNetworkAvailable

        var observableRemote: Observable<CurrentWeatherEntity>? = null
        if (checkConnection) {
            observableRemote = getCurrentWeatherRemote(city)
        }

        val observableLocal = getCurrentWeatherLocal()

        return if (checkConnection) Observable.concatArrayEager(
            observableRemote,
            observableLocal
        )
        else observableLocal
    }

    private fun getCurrentWeatherRemote(city: String): Observable<CurrentWeatherEntity>? =
        remoteDataSource.getCurrentWeather(city, API_KEY)
            ?.doOnNext {
                localDataSource.clearCurrentWeather()
                localDataSource.saveCurrentWeather(it)
            }

    private fun getCurrentWeatherLocal(): Observable<CurrentWeatherEntity>? =
        localDataSource.getCurrentWeather()

    fun saveLocation(location: UserLocationEntity) {
        localDataSource.clearUserLocation()
        localDataSource.saveUserLocation(location)
    }

    fun getWeatherFFD(city: String): Observable<WeatherForFiveDaysEntity>? {
        val checkConnection = NetworkUtil.isNetworkAvailable

        var observableRemote: Observable<WeatherForFiveDaysEntity>? = null
        if (checkConnection) {
            observableRemote = getWeatherFFDRemote(city)
        }

        val observableLocal = getWeatherFFDLocal()

        return if (checkConnection) Observable.concatArrayEager(
            observableRemote,
            observableLocal
        )
        else observableLocal
    }

    private fun getWeatherFFDRemote(city: String): Observable<WeatherForFiveDaysEntity>? =
        remoteDataSource.getWeatherFFD(city, API_KEY)
            ?.doOnNext {
                localDataSource.clearWeatherFFD()
                localDataSource.saveWeatherFFD(it)
            }

    private fun getWeatherFFDLocal(): Observable<WeatherForFiveDaysEntity>? =
        localDataSource.getWeatherFFD()
}
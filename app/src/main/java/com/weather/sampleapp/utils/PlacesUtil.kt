package com.weather.sampleapp.utils

import android.widget.ArrayAdapter
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class PlacesUtil {
    companion object {
        private const val HTTP_REQUEST =
            "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="
        private const val KEY_IS = "&key="
        private const val AUTOCOMPLETE_KEY = "AIzaSyBo17EwUakx3b_-iCvr2EPL-WyDDE1EvMk"
        private const val PREDICTION = "predictions"
        private const val DESCRIPTION = "description"
        private var LANGUAGE: String = "&language=" + Locale.getDefault().language
        private const val TIMEOUT = 5000

        fun autoComplete(input: String): MutableList<String> {
            var result = mutableListOf<String>()
            var httpConnection: HttpURLConnection? = null
            var stringBuilder = StringBuilder()
            try {
                val url = URL(HTTP_REQUEST + input + KEY_IS + AUTOCOMPLETE_KEY + LANGUAGE)
                httpConnection = url.openConnection() as HttpURLConnection
                httpConnection.connectTimeout = TIMEOUT
                var inputStreamReader = InputStreamReader(httpConnection.inputStream)
                var read: Int
                val buff = CharArray(1024)
                while (inputStreamReader.read(buff).let { tmp -> read = tmp;read != -1 }) {
                    stringBuilder.append(buff, 0, read)
                }
//                while (kotlin.run { read = inputStreamReader.read(buff);read != -1 }) {
//                }
            } catch (ex: IOException) {
                ex.printStackTrace()
            } finally {
                httpConnection?.disconnect()
            }
            try {
                val jsonObject = JSONObject(stringBuilder.toString())
                val jsonArray = jsonObject.getJSONArray(PREDICTION)
                for (i in 0 until jsonArray.length()) {
                    result.add(jsonArray.getJSONObject(i).getString(DESCRIPTION))
                }
            } catch (ex: JSONException) {
                ex.printStackTrace()
            }
            return result
        }


        fun getMainText(arrayAdapter: ArrayAdapter<String>, position: Int): String {
            val s = arrayAdapter.getItem(position)
            var stringBuilder = StringBuilder()
            if (s != null) {
                for (char in s) {
                    if (char != ',')
                        stringBuilder.append(char)
                    else break
                }
            }
            return stringBuilder.toString()
        }


    }
}
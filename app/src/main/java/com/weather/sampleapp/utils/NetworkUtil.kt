package com.weather.sampleapp.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import javax.inject.Inject

class NetworkUtil @Inject constructor(var context: Context) {
    companion object {

        var isNetworkAvailable: Boolean = false
    }

    fun setConnectionListener() {
        val connectivity = context.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivity.registerDefaultNetworkCallback(object :
                ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    isNetworkAvailable = true
                }

                override fun onLosing(network: Network, maxMsToLive: Int) {
                    super.onLosing(network, maxMsToLive)
                    isNetworkAvailable = false
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    isNetworkAvailable = false
                }
            })
        } else {
            isNetworkAvailable = true
            //TODO add for previous versions
        }
    }
}
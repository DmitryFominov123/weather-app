package com.weather.sampleapp.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    companion object {
        fun getDate(temp: String): String {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(temp)
            val simpleDateFormat = SimpleDateFormat("dd MMM", Locale.getDefault())

            return simpleDateFormat.format(date!!)
        }

        fun getDate2(temp: String): String {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(temp)
            val simpleDateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())

            return simpleDateFormat.format(date!!)
        }
    }
}
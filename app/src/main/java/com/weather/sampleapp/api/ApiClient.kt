package com.weather.sampleapp.api

import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {

    private lateinit var api: WeatherAPI

    private fun getApi(): WeatherAPI = api


    private object Holder {
        val INSTANCE = ApiClient()
    }

    companion object {
        val instance: ApiClient by lazy { Holder.INSTANCE }
    }

    val BASE_URL: String = "https://api.openweathermap.org/data/2.5/"

    fun init() {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                    .build()
                val response = chain.proceed(request)
                response.cacheResponse()
                response
            }
            .build()


        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        this.api = retrofit.create(WeatherAPI::class.java)
    }

    fun getCurrentWeather(cityName: String, apiKey: String, callback: IRequestCallback<CurrentWeatherEntity>) {
        val call = getApi().getCurrentWeather(cityName, apiKey)
//        val request = ApiRequest(call)
//        request.send(callback)
    }

    fun getWeatherTo5Days(
        cityName: String,
        apiKey: String,
        callback: IRequestCallback<WeatherForFiveDaysEntity>
    ) {
        val call = getApi().getFFDWeather(cityName, apiKey)
//        val request = ApiRequest(call)
//        request.send(callback)
    }
}
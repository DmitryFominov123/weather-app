package com.weather.sampleapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.weather.sampleapp.databinding.ItemRecyclerViewBinding
import com.weather.sampleapp.db.entity.weatherforfivedays.ListBas
import com.weather.sampleapp.utils.ConstUtil
import com.weather.sampleapp.utils.DateUtil
import kotlin.math.round

class WeatherAdapter(val mItems: List<ListBas> = mutableListOf()) :
    RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val context = parent.context
        return WeatherViewHolder(
            ItemRecyclerViewBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            ), context
        )
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    inner class WeatherViewHolder(
        private val binding: ItemRecyclerViewBinding,
        private val context: Context
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(position: Int) {
            val item = mItems[position]
            val avgTemp =
                round(item.main.temp_max - (item.main.temp_max - item.main.temp_min) / 2 - 273.15F).toInt()
            val avgTempString = "$avgTemp\u00B0"
            binding.avgTempTextView.text = avgTempString
            binding.timeTextView.text = DateUtil.getDate2(item.dt_txt)
            setAvatar(item.weather[0].icon)
        }

        private fun setAvatar(pPath: String) {
            Glide.with(context)
                .load(ConstUtil.OPENWEATHERMAP_ICON + pPath + ConstUtil.OPENWEATHERMAP_END)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .apply(RequestOptions.centerCropTransform())
                .into(binding.stateImageView)
        }
    }
}

package com.weather.sampleapp.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.weather.sampleapp.di.DaggerViewModelFactory
import com.weather.sampleapp.di.ViewModelKey
import com.weather.sampleapp.di.scopes.ApplicationScope
import com.weather.sampleapp.viewmodel.WeatherViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(WeatherViewModel::class)
    internal abstract fun bindWeatherViewModel(viewModel: WeatherViewModel): ViewModel

    @Binds
    @ApplicationScope
    internal abstract fun bindViewModelFactory(factory: DaggerViewModelFactory): ViewModelProvider.Factory
}
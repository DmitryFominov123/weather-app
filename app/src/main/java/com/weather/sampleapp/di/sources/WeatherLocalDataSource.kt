package com.weather.sampleapp.di.sources


import com.weather.sampleapp.db.dao.CurrentWeatherDao
import com.weather.sampleapp.db.dao.ProfileDao
import com.weather.sampleapp.db.dao.UserLocationDao
import com.weather.sampleapp.db.dao.WeatherForFiveDaysDao
import com.weather.sampleapp.db.entity.ProfileEntity
import com.weather.sampleapp.db.entity.UserLocationEntity
import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity
import com.weather.sampleapp.di.scopes.ApplicationScope
import com.weather.sampleapp.schedulers.BaseSchedulerProvider
import io.reactivex.Observable
import javax.inject.Inject


/**
 * Concrete implementation of the Local Data Source
 */
@ApplicationScope
class WeatherLocalDataSource @Inject constructor(
    private val currentWeatherDao: CurrentWeatherDao?,
    private val userLocationDao: UserLocationDao?,
    private val weatherForFiveDaysDao: WeatherForFiveDaysDao?,
    private val profileDao: ProfileDao?,
    val schedulerProvider: BaseSchedulerProvider?
) : IWeatherLocalDataSource {

    override fun getCurrentWeather(): Observable<CurrentWeatherEntity>? =
        currentWeatherDao?.getCurrentWeather()

    override fun getUserById(id: Int): Observable<ProfileEntity>? = profileDao?.getUserById(id)

    override fun getLastUserLocation(): Observable<UserLocationEntity>? =
        userLocationDao?.getLastUserLocation()

    override fun getWeatherFFD(): Observable<WeatherForFiveDaysEntity>? =
        weatherForFiveDaysDao?.getWeatherFFD()

    override fun saveCurrentWeather(currentWeatherEntity: CurrentWeatherEntity): Unit? =
        currentWeatherDao?.insert(currentWeatherEntity)

    override fun saveUserLocation(userLocationEntity: UserLocationEntity) {
        userLocationDao?.insert(userLocationEntity)
    }

    override fun saveWeatherFFD(weatherForFiveDaysEntity: WeatherForFiveDaysEntity) {
        weatherForFiveDaysDao?.insert(weatherForFiveDaysEntity)
    }

    override fun clearWeatherFFD() {
        weatherForFiveDaysDao?.clearDatabase()
    }

    override fun clearCurrentWeather() {
        currentWeatherDao?.clearDatabase()
    }

    override fun clearUserLocation() {
        userLocationDao?.clearDatabase()
    }
}

package com.weather.sampleapp.di.modules

import androidx.room.RoomDatabase
import com.weather.sampleapp.db.WeatherDatabase
import com.weather.sampleapp.di.scopes.WeatherScope
import dagger.Binds
import dagger.Module

@Module
abstract class UtilModule {

    @Binds
    @WeatherScope
    abstract fun bindsUtils(weatherViewModel: WeatherDatabase): RoomDatabase
}
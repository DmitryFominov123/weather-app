package com.weather.sampleapp.di.modules

import com.weather.sampleapp.di.scopes.WeatherScope
import com.weather.sampleapp.ui.activity.WeatherActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @WeatherScope
    @ContributesAndroidInjector(modules = [(WeatherModule::class)])
    abstract fun bindWeatherActivity(): WeatherActivity
}
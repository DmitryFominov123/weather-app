package com.weather.sampleapp.di.modules

import android.content.Context
import com.weather.sampleapp.App
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {
    @Provides
    fun provideContext(): Context {
        return App.instance
    }
}
package com.weather.sampleapp.di.sources

import com.weather.sampleapp.db.entity.ProfileEntity
import com.weather.sampleapp.db.entity.UserLocationEntity
import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity
import io.reactivex.Observable
import io.reactivex.Single

interface IWeatherLocalDataSource {

    fun getCurrentWeather(): Observable<CurrentWeatherEntity>?
    fun getUserById(id: Int): Observable<ProfileEntity>?
    fun getLastUserLocation(): Observable<UserLocationEntity>?
    fun getWeatherFFD(): Observable<WeatherForFiveDaysEntity>?

    fun saveCurrentWeather(currentWeatherEntity: CurrentWeatherEntity): Unit?
    fun saveUserLocation(userLocationEntity: UserLocationEntity)
    fun saveWeatherFFD(weatherForFiveDaysEntity: WeatherForFiveDaysEntity)

    fun clearWeatherFFD()
    fun clearCurrentWeather()
    fun clearUserLocation()
}
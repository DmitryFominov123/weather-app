package com.weather.sampleapp.di

import android.app.Application
import androidx.room.Room
import com.weather.sampleapp.db.WeatherDatabase
import com.weather.sampleapp.db.dao.CurrentWeatherDao
import com.weather.sampleapp.db.dao.ProfileDao
import com.weather.sampleapp.db.dao.UserLocationDao
import com.weather.sampleapp.db.dao.WeatherForFiveDaysDao
import com.weather.sampleapp.di.scopes.ApplicationScope
import com.weather.sampleapp.schedulers.BaseSchedulerProvider
import com.weather.sampleapp.schedulers.SchedulerProvider
import com.weather.sampleapp.utils.ConstUtil
import dagger.Module
import dagger.Provides


@Module
class WeatherLocalDataModule {

    @ApplicationScope
    @Provides
    fun provideDb(context: Application): WeatherDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            WeatherDatabase::class.java, ConstUtil.DATABASE_NAME
        )
            .build()
    }

    @ApplicationScope
    @Provides
    fun provideCurrentWeatherDao(db: WeatherDatabase): CurrentWeatherDao {
        return db.getCurrentWeatherDao()
    }

    @ApplicationScope
    @Provides
    fun provideUserLocationDao(db: WeatherDatabase): UserLocationDao {
        return db.getUserLocationDao()
    }

    @ApplicationScope
    @Provides
    fun provideWeatherFor5DaysDao(db: WeatherDatabase): WeatherForFiveDaysDao {
        return db.getWeatherForFiveDaysDao()
    }

    @ApplicationScope
    @Provides
    fun provideProfilesDao(db: WeatherDatabase): ProfileDao {
        return db.getProfileDao()
    }

    @ApplicationScope
    @Provides
    fun provideSchedulerProvider(): BaseSchedulerProvider? {
        return SchedulerProvider.instance
    }
}
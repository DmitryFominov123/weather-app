package com.weather.sampleapp.di.modules

import com.weather.sampleapp.di.scopes.WeatherScope
import com.weather.sampleapp.utils.NetworkUtil
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UtilBindingModule {

    @WeatherScope
    @ContributesAndroidInjector(modules = [(ApplicationModule::class)])
    abstract fun provideUtils(): NetworkUtil
}
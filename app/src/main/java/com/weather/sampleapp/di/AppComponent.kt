package com.weather.sampleapp.di

import android.app.Application
import com.weather.sampleapp.App
import com.weather.sampleapp.di.modules.ActivityBindingModule
import com.weather.sampleapp.di.modules.ApplicationModule
import com.weather.sampleapp.di.modules.ViewModelModule
import com.weather.sampleapp.di.scopes.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@ApplicationScope
@Component(
    modules = [ApplicationModule::class, ActivityBindingModule::class,
        WeatherRepositoryModule::class, AndroidSupportInjectionModule::class, ViewModelModule::class]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(app: App)
}
package com.weather.sampleapp.db.entity.currentweather

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("message")
    var error: String? = null,
    @SerializedName("cod")
    var errorCode: Int = -1
)
package com.weather.sampleapp.db.entity.currentweather

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

import com.google.gson.annotations.SerializedName
import com.weather.sampleapp.db.converter.ListBasConverter
import com.weather.sampleapp.db.entity.generalweather.Main
import com.weather.sampleapp.db.entity.generalweather.Sys
import com.weather.sampleapp.db.entity.generalweather.Wind

@Entity
data class CurrentWeatherEntity(
    @Embedded
    @SerializedName("coord") val coord: Coord,
    @TypeConverters(ListBasConverter::class)
    @SerializedName("weather") val weather: List<Weather>,
    @SerializedName("base") val base: String,
    @Embedded
    @SerializedName("main") val main: Main,
    @SerializedName("visibility") val visibility: Int,
    @Embedded
    @SerializedName("wind") val wind: Wind,
    @Embedded
    @SerializedName("clouds") val clouds: Clouds,
    @SerializedName("dt") val dt: Int,
    @Embedded(prefix = "prefix_")
    @SerializedName("sys") val sys: Sys,
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("cod") val cod: Int
) {
    @PrimaryKey(autoGenerate = true)
    var uId: Int? = null
}
package com.weather.sampleapp.db.entity.currentweather

import com.google.gson.annotations.SerializedName

data class Clouds(

	@SerializedName("all") val all: Int
)
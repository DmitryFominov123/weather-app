package com.weather.sampleapp.db.converter;

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.weather.sampleapp.db.entity.weatherforfivedays.ListBas

class ListBasConverter {

    @TypeConverter
    fun fromListToString(list: List<ListBas>): String {
        val gson = Gson()
        val type = object : TypeToken<List<ListBas>>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun fromStringToList(string: String): List<ListBas> {
        val gson = Gson()
        val type = object : TypeToken<List<ListBas>>() {}.type
        return gson.fromJson(string, type)
    }
}
package com.weather.sampleapp.db.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.weather.sampleapp.db.entity.currentweather.Weather
import com.weather.sampleapp.db.entity.weatherforfivedays.ListBas

class WeatherConverter {

    @TypeConverter
    fun fromListToString(list: List<Weather>): String {
        val gson = Gson()
        val type = object : TypeToken<List<Weather>>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun fromStringToList(string: String): List<Weather> {
        val gson = Gson()
        val type = object : TypeToken<List<Weather>>() {}.type
        return gson.fromJson(string, type)
    }
}
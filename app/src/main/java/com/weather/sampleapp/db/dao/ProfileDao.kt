package com.weather.sampleapp.db.dao

import androidx.room.*
import com.weather.sampleapp.db.entity.ProfileEntity
import io.reactivex.Maybe
import io.reactivex.Observable

@Dao
interface ProfileDao {

    @Query("SELECT * FROM ProfileEntity")
    fun getUsers(): Observable<List<ProfileEntity>>

    @Query("SELECT * FROM ProfileEntity WHERE uId=:id")
    fun getUserById(id: Int): Observable<ProfileEntity>

    @Insert
    fun insert(profileEntity: ProfileEntity)

    @Update
    fun update(profileEntity: ProfileEntity)

    @Delete
    fun delete(profileEntity: ProfileEntity)
}
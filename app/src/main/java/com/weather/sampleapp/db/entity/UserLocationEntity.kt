package com.weather.sampleapp.db.entity;

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserLocationEntity(
    var timestamp: Long,
    var city: String
) {
    @PrimaryKey(autoGenerate = true)
    var uId: Int? = null

    constructor() : this(0L, "")
}
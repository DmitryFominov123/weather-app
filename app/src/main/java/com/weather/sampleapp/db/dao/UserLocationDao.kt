package com.weather.sampleapp.db.dao

import androidx.room.*
import com.weather.sampleapp.db.entity.UserLocationEntity
import io.reactivex.Maybe
import io.reactivex.Observable

@Dao
interface UserLocationDao {

    @Query("SELECT * FROM UserLocationEntity")
    fun getAllUsersLocations(): Observable<List<UserLocationEntity>>

    @Query("SELECT * FROM UserLocationEntity ORDER BY timestamp DESC LIMIT 1")
    fun getLastUserLocation(): Observable<UserLocationEntity>

    @Insert
    fun insert(userLocationEntity: UserLocationEntity)

    @Update
    fun update(userLocationEntity: UserLocationEntity)

    @Delete
    fun delete(userLocationEntity: UserLocationEntity)

    @Query("DELETE FROM UserLocationEntity")
    fun clearDatabase()
}